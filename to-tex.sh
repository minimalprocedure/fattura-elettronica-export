#!/bin/sh
pandoc \
--columns=2 \
--template=template.latex \
$1 \
-o $1.tex

