#!/bin/sh
pandoc \
--columns=20 \
--from=markdown+pipe_tables \
--template=template.latex \
$1 \
--pdf-engine=xelatex \
-o $1.pdf

