﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.1" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:a="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2">
	<xsl:output method="text" encoding='utf-8' />

	<xsl:strip-space elements="*"/>

	<xsl:variable name="LF" select="'&#10;'" />
	<xsl:variable name="DLF" select="'&#10;&#10;'" />
	<xsl:variable name="bMark" select="'**'" />
	<xsl:variable name="P" select="'|'" />
	<xsl:variable name="SPC" select="'&#032;'" />
	<xsl:variable name="PSEP" select="'&#126;'" />

	<xsl:template name="FormatTD">
		<xsl:param name="TD" />
		<xsl:choose>
			<xsl:when test="$TD='TD01'">
				<xsl:text>fattura</xsl:text>
			</xsl:when>
			<xsl:when test="$TD='TD02'">
				<xsl:text>acconto/anticipo su fattura</xsl:text>
			</xsl:when>
			<xsl:when test="$TD='TD03'">
				<xsl:text>acconto/anticipo su parcella</xsl:text>
			</xsl:when>
			<xsl:when test="$TD='TD04'">
				<xsl:text>nota di credito</xsl:text>
			</xsl:when>
			<xsl:when test="$TD='TD05'">
				<xsl:text>nota di debito</xsl:text>
			</xsl:when>
			<xsl:when test="$TD='TD06'">
				<xsl:text>parcella</xsl:text>
			</xsl:when>
			<xsl:when test="$TD=''">
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>codice non previsto</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="FormatEI">
		<xsl:param name="EI" />
		<xsl:choose>
			<xsl:when test="$EI='I'">
				<xsl:text>esigibilità immediata</xsl:text>
			</xsl:when>
			<xsl:when test="$EI='D'">
				<xsl:text>esigibilità differita</xsl:text>
			</xsl:when>
			<xsl:when test="$EI='S'">
				<xsl:text>scissione dei pagamenti</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>codice non previsto</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="FormatCP">
		<xsl:param name="CP" />
		<xsl:choose>
			<xsl:when test="$CP='TP01'">
				<xsl:text>pagamento a rate</xsl:text>
			</xsl:when>
			<xsl:when test="$CP='TP02'">
				<xsl:text>pagamento completo</xsl:text>
			</xsl:when>
			<xsl:when test="$CP='TP03'">
				<xsl:text>anticipo</xsl:text>
			</xsl:when>
			<xsl:when test="$CP=''">
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>codice non previsto</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="FormatMP">
		<xsl:param name="MP" />

		<xsl:choose>
			<xsl:when test="$MP='MP01'">
				<xsl:text>contanti</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP02'">
				<xsl:text>assegno</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP03'">
				<xsl:text>assegno circolare</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP04'">
				<xsl:text>contanti presso Tesoreria</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP05'">
				<xsl:text>bonifico</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP06'">
				<xsl:text>vaglia cambiario</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP07'">
				<xsl:text>bollettino bancario</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP08'">
				<xsl:text>carta di pagamento</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP09'">
				<xsl:text>RID</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP10'">
				<xsl:text>RID utenze</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP11'">
				<xsl:text>RID veloce</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP12'">
				<xsl:text>RIBA</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP13'">
				<xsl:text>MAV</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP14'">
				<xsl:text>quietanza erario</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP15'">
				<xsl:text>giroconto su conti di contabilità speciale</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP16'">
				<xsl:text>domiciliazione bancaria</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP17'">
				<xsl:text>domiciliazione postale</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP18'">
				<xsl:text>bollettino di c/c postale</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP19'">
				<xsl:text>SEPA Direct Debit</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP20'">
				<xsl:text>SEPA Direct Debit CORE</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP21'">
				<xsl:text>SEPA Direct Debit B2B</xsl:text>
			</xsl:when>
			<xsl:when test="$MP='MP22'">
				<xsl:text>Trattenuta su somme già riscosse</xsl:text>
			</xsl:when>
			<xsl:when test="$MP=''">
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>codice non previsto</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="FormatRF">
		<xsl:param name="RF" />
		<xsl:choose>
			<xsl:when test="$RF='RF01'">
				<xsl:text>ordinario</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF02'">
				<xsl:text>contribuenti minimi</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF03'">
				<xsl:text>nuove iniziative produttive - Non più valido in quanto abrogato dalla legge di stabilità 2015</xsl:text>																	
			</xsl:when>
			<xsl:when test="$RF='RF04'">
				<xsl:text>agricoltura e attività connesse e pesca</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF05'">
				<xsl:text>vendita sali e tabacchi</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF06'">
				<xsl:text>commercio fiammiferi</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF07'">
				<xsl:text>editoria</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF08'">
				<xsl:text>gestione servizi telefonia pubblica</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF09'">
				<xsl:text>rivendita documenti di trasporto pubblico e di sosta</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF10'">
				<xsl:text>intrattenimenti, giochi e altre attività di cui alla tariffa allegata al DPR 640/72</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF11'">
				<xsl:text>agenzie viaggi e turismo</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF12'">
				<xsl:text>agriturismo</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF13'">
				<xsl:text>vendite a domicilio</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF14'">
				<xsl:text>rivendita beni usati, oggetti d’arte,	d’antiquariato o da collezione</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF15'">
				<xsl:text>agenzie di vendite all’asta di oggetti d’arte,	antiquariato o da collezione</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF16'">
				<xsl:text>IVA per cassa P.A.</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF17'">
				<xsl:text>IVA per cassa - art. 32-bis, D.L. 83/2012</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF19'">
				<xsl:text>Regime forfettario</xsl:text>
			</xsl:when>
			<xsl:when test="$RF='RF18'">
				<xsl:text>altro</xsl:text>
			</xsl:when>
			<xsl:when test="$RF=''">
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>codice non previsto</xsl:text>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template name="FormatDate">
		<xsl:param name="DateTime" />

		<xsl:variable name="year" select="substring($DateTime,1,4)" />
		<xsl:variable name="month" select="substring($DateTime,6,2)" />
		<xsl:variable name="day" select="substring($DateTime,9,2)" />

		<xsl:value-of select="$day" />
		<xsl:value-of select="'/'" />

		<xsl:choose>
			<xsl:when test="string-length($month) = 1">
				<xsl:text>0</xsl:text>
				<xsl:value-of select="$month" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$month" />
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:value-of select="'/'" />
		<xsl:value-of select="$year" />

		<xsl:variable name="time" select="substring($DateTime,12)" />
		<xsl:if test="$time != ''">
			<xsl:variable name="hh" select="substring($time,1,2)" />
			<xsl:variable name="mm" select="substring($time,4,2)" />
			<xsl:variable name="ss" select="substring($time,7,2)" />

			<xsl:value-of select="' '" />
			<xsl:value-of select="$hh" />
			<xsl:value-of select="':'" />
			<xsl:value-of select="$mm" />
			<xsl:value-of select="':'" />
			<xsl:value-of select="$ss" />
		</xsl:if>
	</xsl:template>

	<xsl:template name="Sede">
		<xsl:value-of select="Sede/Indirizzo" />
		<xsl:text> - </xsl:text>
		<xsl:value-of select="Sede/CAP" />
		<xsl:text>, </xsl:text>
		<xsl:value-of select="Sede/Comune" />
		<xsl:text> (</xsl:text>
		<xsl:value-of select="Sede/Provincia" />
		<xsl:text>) - </xsl:text>
		<xsl:value-of select="Sede/Nazione" />
	</xsl:template>

	<xsl:template name="FormatListItem">		
		<xsl:param name="Key" />
		<xsl:param name="Value" />
		<xsl:text>  + </xsl:text>
		<xsl:value-of select='$Key'/>
		<xsl:text>: **</xsl:text>
		<xsl:value-of select='$Value'/>
		<xsl:text>**</xsl:text>
		<xsl:value-of select='$LF'/>
	</xsl:template>

	<xsl:template name="header">

		<xsl:text>---</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>title: Fattura Elettronica</xsl:text>
		<xsl:value-of select='$LF'/>
		<xsl:text>lang: it</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>toc: true</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>toc-depth: 5</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>numbersections: true</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>papersize: a4</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>documentclass: article</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>mainfont: Arial</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>fontsize: 8.5pt</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>geometry: a4paper,left=24mm,right=24mm,top=30mm,bottom=30mm</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>links-as-notes: true</xsl:text><xsl:value-of select='$LF'/>
		<xsl:text>---</xsl:text><xsl:value-of select='$DLF'/>

		<xsl:text># Fattura Elettronica</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:text>_Copia di cortesia ad uso esclusivamente di consultazione._</xsl:text>
		<xsl:value-of select='$DLF'/>
	</xsl:template>

	<xsl:template match="FatturaElettronicaHeader/DatiTrasmissione">

		<xsl:call-template name="header" />	

		<xsl:text>## Dati Trasmissione</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:call-template name="FormatListItem">	
			<xsl:with-param name="Key">Progressivo di invio</xsl:with-param>
			<xsl:with-param name="Value" select="ProgressivoInvio" />
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Identificativo del trasmittente</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="IdTrasmittente/IdPaese" />
				<xsl:value-of select="IdTrasmittente/IdCodice" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Formato Trasmissione</xsl:with-param>
			<xsl:with-param name="Value" select="FormatoTrasmissione" />
		</xsl:call-template>	

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Codice identificativo destinatario</xsl:with-param>
			<xsl:with-param name="Value" select="CodiceDestinatario" />
		</xsl:call-template>

		<xsl:if test="PECDestinatario">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">PEC</xsl:with-param>
				<xsl:with-param name="Value" select="PECDestinatario" />
			</xsl:call-template>	
		</xsl:if>	

		<xsl:if test="ContattiTrasmittente">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Telefono</xsl:with-param>
				<xsl:with-param name="Value" select="ContattiTrasmittente/Telefono" />
			</xsl:call-template>
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Email</xsl:with-param>
				<xsl:with-param name="Value" select="ContattiTrasmittente/Email" />
			</xsl:call-template>
		</xsl:if>	

		<xsl:value-of select='$DLF'/>

	</xsl:template>

	<xsl:template match="FatturaElettronicaHeader/CedentePrestatore">
		<xsl:text>## Dati del cedente/prestatore</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:variable name="rf">
			<xsl:value-of select="DatiAnagrafici/RegimeFiscale" />
			<xsl:text> _(</xsl:text>
			<xsl:call-template name="FormatRF">
				<xsl:with-param name="RF" select="DatiAnagrafici/RegimeFiscale" />
			</xsl:call-template>
			<xsl:text>)_</xsl:text>
		</xsl:variable>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Denominazione</xsl:with-param>
			<xsl:with-param name="Value" select="DatiAnagrafici/Anagrafica/Denominazione" />
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Sede</xsl:with-param>
				<xsl:with-param name="Value">
				<xsl:call-template name="Sede" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:if test="DatiAnagrafici/IdFiscaleIVA">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Partita i.v.a.</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="DatiAnagrafici/IdFiscaleIVA/IdPaese" />
					<xsl:value-of select="DatiAnagrafici/IdFiscaleIVA/IdCodice" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="DatiAnagrafici/CodiceFiscale">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Fiscale</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/CodiceFiscale" />
			</xsl:call-template>
		</xsl:if>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Regime fiscale</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="$rf" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:if test="DatiAnagrafici/Anagrafica/Nome">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Nome</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="DatiAnagrafici/Anagrafica/Titolo" />
					<xsl:value-of select="$SPC" />
					<xsl:value-of select="DatiAnagrafici/Anagrafica/Nome" />
					<xsl:value-of select="$SPC" />
					<xsl:value-of select="DatiAnagrafici/Anagrafica/Cognome" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if >

		<xsl:if test="DatiAnagrafici/Anagrafica/CodEORI">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice EORI</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/Anagrafica/CodEORI" />
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="DatiAnagrafici/AlboProfessionale">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Albo professionale</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/AlboProfessionale" />
			</xsl:call-template>
		</xsl:if >

		<xsl:if test="DatiAnagrafici/ProvinciaAlbo">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Provincia albo</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/ProvinciaAlbo" />
			</xsl:call-template>
		</xsl:if >

		<xsl:if test="DatiAnagrafici/NumeroIscrizioneAlbo">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Numero iscrizione albo</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/NumeroIscrizioneAlbo" />
			</xsl:call-template>
		</xsl:if >

		<xsl:if test="DatiAnagrafici/DataIscrizioneAlbo">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Data iscrizione albo</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/DataIscrizioneAlbo" />
			</xsl:call-template>
		</xsl:if >

		<xsl:value-of select='$DLF'/>
	</xsl:template>

	<xsl:template match="FatturaElettronicaHeader/CessionarioCommittente">
		<xsl:text>## Dati del cessionario/committente</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Denominazione</xsl:with-param>
			<xsl:with-param name="Value" select="DatiAnagrafici/Anagrafica/Denominazione" />
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Sede</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:call-template name="Sede" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:if test="DatiAnagrafici/IdFiscaleIVA">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Partita i.v.a.</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="DatiAnagrafici/IdFiscaleIVA/IdPaese" />
					<xsl:value-of select="DatiAnagrafici/IdFiscaleIVA/IdCodice" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="DatiAnagrafici/CodiceFiscale">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Fiscale</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/CodiceFiscale" />
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="DatiAnagrafici/Anagrafica/Nome">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Nome</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="DatiAnagrafici/Anagrafica/Titolo" />
					<xsl:value-of select="$SPC" />
					<xsl:value-of select="DatiAnagrafici/Anagrafica/Nome" />
					<xsl:value-of select="$SPC" />
					<xsl:value-of select="DatiAnagrafici/Anagrafica/Cognome" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if >

		<xsl:if test="DatiAnagrafici/Anagrafica/CodEORI">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice EORI</xsl:with-param>
				<xsl:with-param name="Value" select="DatiAnagrafici/Anagrafica/CodEORI" />
			</xsl:call-template>
		</xsl:if>

		<xsl:value-of select='$DLF'/>
	</xsl:template>

	<xsl:template name="Causali">
		<xsl:for-each select="Causale">
			<xsl:text>|</xsl:text>
			<xsl:value-of select="position()" />|<xsl:value-of select="text()" />
			<xsl:text>|</xsl:text>
			<xsl:value-of select='$LF'/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="FatturaElettronicaBody/DatiGenerali/DatiGeneraliDocumento">
		<xsl:text>## Dati generali del documento</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:variable name="d">
			<xsl:call-template name="FormatDate">
				<xsl:with-param name="DateTime" select="Data" />
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name="td">
			<xsl:value-of select="TipoDocumento" />
			<xsl:text> _(</xsl:text>
			<xsl:call-template name="FormatTD">
				<xsl:with-param name="TD" select="TipoDocumento" />
			</xsl:call-template>
			<xsl:text>)_</xsl:text>
		</xsl:variable>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Data documento</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="$d" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Tipologia documento</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="$td" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Valuta importi</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="Divisa" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:value-of select='$DLF'/>
		<xsl:text>### Causali </xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:text>|**Nr.**|**Descrizione**|</xsl:text>
		<xsl:value-of select='$LF'/>
	  <xsl:text>|:-:|--------------------|</xsl:text>
		<xsl:value-of select='$LF'/>
		<xsl:call-template name="Causali" />

		<xsl:value-of select='$DLF'/>

	</xsl:template>

	<xsl:template match="FatturaElettronicaBody/DatiGenerali/DatiOrdineAcquisto">
		<xsl:text>## Dati dell'ordine di acquisto</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Identificativo ordine di acquisto</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="IdDocumento" />
			</xsl:with-param>
		</xsl:call-template>
		
		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea di fattura a cui si riferisce</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="RiferimentoNumeroLinea" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea ordine di acquisto</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="NumItem" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:if test="CodiceCIG">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Identificativo Gara (CIG)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCIG" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="CodiceCUP">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Unitario Progetto (CUP)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCUP" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:value-of select='$DLF'/>
	</xsl:template>

	<xsl:template match="FatturaElettronicaBody/DatiGenerali/DatiContratto">
		<xsl:text>## Dati del contratto</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:variable name="d">
			<xsl:call-template name="FormatDate">
				<xsl:with-param name="DateTime" select="Data" />
			</xsl:call-template>
		</xsl:variable>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Identificativo contratto</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="IdDocumento" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Data</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="$d" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea di fattura a cui si riferisce</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="RiferimentoNumeroLinea" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea contratto</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="NumItem" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:if test="CodiceCIG">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Identificativo Gara (CIG)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCIG" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="CodiceCUP">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Unitario Progetto (CUP)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCUP" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
		
		<xsl:value-of select='$DLF'/>
	</xsl:template>

	<xsl:template match="FatturaElettronicaBody/DatiGenerali/DatiConvenzione">
		<xsl:text>## Dati della convenzione</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Identificativo convenzione</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="IdDocumento" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea di fattura a cui si riferisce</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="RiferimentoNumeroLinea" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea convenzione</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="NumItem" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:if test="CodiceCIG">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Identificativo Gara (CIG)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCIG" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="CodiceCUP">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Unitario Progetto (CUP)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCUP" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:value-of select='$DLF'/>
	</xsl:template>

	<xsl:template match="FatturaElettronicaBody/DatiGenerali/DatiRicezione">
		<xsl:text>## Dati della convenzione</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Identificativo ricezione</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="IdDocumento" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea di fattura a cui si riferisce</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="RiferimentoNumeroLinea" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Numero linea ricezione</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="NumItem" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:if test="CodiceCIG">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Identificativo Gara (CIG)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCIG" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="CodiceCUP">
			<xsl:call-template name="FormatListItem">
				<xsl:with-param name="Key">Codice Unitario Progetto (CUP)</xsl:with-param>
				<xsl:with-param name="Value">
					<xsl:value-of select="CodiceCUP" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:value-of select='$DLF'/>
	</xsl:template>

	<xsl:template match="FatturaElettronicaBody/DatiGenerali/DatiTrasporto">
		<xsl:text>## Dati relativi al trasporto</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:variable name="d">
			<xsl:call-template name="FormatDate">
				<xsl:with-param name="DateTime" select="DataOraConsegna" />
			</xsl:call-template>
		</xsl:variable>

		<xsl:text>### Dati del vettore</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Denominazione</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="DatiAnagraficiVettore/Anagrafica/Denominazione" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Identificativo fiscale ai fini IVA</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="DatiAnagraficiVettore/IdFiscaleIVA/IdPaese" />
				<xsl:value-of select="DatiAnagraficiVettore/IdFiscaleIVA/IdCodice" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="FormatListItem">
			<xsl:with-param name="Key">Data e ora di consegna</xsl:with-param>
			<xsl:with-param name="Value">
				<xsl:value-of select="$d" />
			</xsl:with-param>
		</xsl:call-template>

		<xsl:value-of select='$DLF'/>

	</xsl:template>

	<xsl:template name="DatiBeniServizi-Line">
		<xsl:text>|</xsl:text>
		<xsl:value-of select="NumeroLinea" />|<xsl:value-of select="Descrizione" />|<xsl:value-of select="Quantita" />|<xsl:value-of select="PrezzoUnitario" />|<xsl:value-of select="PrezzoTotale" />|<xsl:value-of select="AliquotaIVA" />
		<xsl:text>|</xsl:text>
	</xsl:template>

		<xsl:variable name="tsep">
			<xsl:text>|</xsl:text>
			<xsl:value-of select='$PSEP'/>|<xsl:value-of select='$PSEP'/>|<xsl:value-of select='$PSEP'/>|<xsl:value-of select='$PSEP'/>|<xsl:value-of select='$PSEP'/>|<xsl:value-of select='$PSEP'/>
			<xsl:text>|</xsl:text>
		</xsl:variable>

		<xsl:variable name="ColumnsSizesServices">
			<xsl:value-of select='$LF'/>
			<xsl:text>|:-:|----------------------|:-------:|:----:|:---:|:--:|</xsl:text>
			<xsl:value-of select='$LF'/>
		</xsl:variable>

	<xsl:template match="FatturaElettronicaBody/DatiBeniServizi">
		<xsl:text>## Dati della fornitura, riepilogo e condizioni di pagamento</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:text>### Fornitura beni o servizi</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:text>|**Nr.**|**Descrizione**|**Quantità**|**Unitario**|**Totale**|**IVA(%)**|</xsl:text>
		<xsl:value-of select='$ColumnsSizesServices'/>
		<xsl:for-each select="DettaglioLinee">
			<xsl:call-template name="DatiBeniServizi-Line" />
			<xsl:value-of select='$LF'/>
		</xsl:for-each>

		<xsl:variable name="ei">
			<xsl:value-of select="DatiRiepilogo/EsigibilitaIVA" />
			<xsl:text> _(</xsl:text>
			<xsl:call-template name="FormatEI">
				<xsl:with-param name="EI" select="DatiRiepilogo/EsigibilitaIVA" />
			</xsl:call-template>
			<xsl:text>)_</xsl:text>
		</xsl:variable>

		<xsl:value-of select='$DLF'/>

		<xsl:text>### Riepilogo</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:text>| |**Esigibilità IVA**|  |**Imposta**|**Imponibile**|**IVA(%)**|</xsl:text>
		<xsl:value-of select='$ColumnsSizesServices'/>
		<xsl:text>|</xsl:text>
		<xsl:value-of select="$SPC" />|<xsl:value-of select="$ei" />|<xsl:value-of select="$SPC" />|<xsl:value-of select="DatiRiepilogo/Imposta" />| <xsl:value-of select="DatiRiepilogo/ImponibileImporto" />|<xsl:value-of select="DatiRiepilogo/AliquotaIVA" />
		<xsl:text>|</xsl:text>

	</xsl:template>	

	<xsl:template match="FatturaElettronicaBody/DatiPagamento">
		<xsl:variable name="d">
			<xsl:call-template name="FormatDate">
				<xsl:with-param name="DateTime" select="DettaglioPagamento/DataScadenzaPagamento" />
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name="mp">
			<xsl:value-of select="DettaglioPagamento/ModalitaPagamento" />
			<xsl:text> _(</xsl:text>
			<xsl:call-template name="FormatMP">
				<xsl:with-param name="MP" select="DettaglioPagamento/ModalitaPagamento" />
			</xsl:call-template>
			<xsl:text>)_</xsl:text>
		</xsl:variable>

		<xsl:variable name="cp">
			<xsl:value-of select="CondizioniPagamento" />
			<xsl:text> _(</xsl:text>
			<xsl:call-template name="FormatCP">
				<xsl:with-param name="CP" select="CondizioniPagamento" />
			</xsl:call-template>
			<xsl:text>)_</xsl:text>
		</xsl:variable>

		<xsl:value-of select='$DLF'/>

		<xsl:text>### Condizioni di pagamento</xsl:text>
		<xsl:value-of select='$DLF'/>

		<xsl:text>| |**Modalità**| | **Condizioni**|**Scadenza**|**Importo**|</xsl:text>
		<xsl:value-of select='$ColumnsSizesServices'/>
		<xsl:text>|</xsl:text>
		<xsl:value-of select="$SPC" />|<xsl:value-of select="$mp" />|<xsl:value-of select="$SPC" />|<xsl:value-of select="$cp" />|<xsl:value-of select="$d" />|<xsl:value-of select="DettaglioPagamento/ImportoPagamento" /> 
		<xsl:text>|</xsl:text>
		<xsl:value-of select='$DLF'/>
	</xsl:template>	

</xsl:stylesheet>
