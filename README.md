Esportazione della fattura elettronica in formato HTML e PDF.

- Installare https://wkhtmltopdf.org
- Installare XSLT da http://xmlsoft.org/

Il file xslt per la conversione è quello ufficiale https://www.fatturapa.gov.it/export/fatturazione/it/a-3.htm

Solo una modifica è stata fatta per caricare un CSS esterno (fepa.css) per l'override delle classi esistenti.
